To configure at III server do the next steps:
* configure options files list in options*.properties (could be done any time later at server, but all files should exist!!!)
* `gradle bTF bASF bDSF` - build 3 folders of app - for local launch, for app server and for db server
* go to /build/scanopt/*
* copy scanopts folder to /iii/iiihome/ at III server
* set up autostart of application:
    x uoptions -si -> T -> V
    add something like:
    # scanopts (scanning option changes for developers)
    sys218:4:respawn:iiiboot -d/dev/null -p$JAVA8_HOME/bin/java -- -jar /iii/iiihome/scanopts/scanopts.jar -s
* if you face problems with access to any of folder or subfolder in /iii/iiihome/scanopts enter as root:
chmod -R 777 /iii/iiihome/scanopts
but better not to do it because git could recognize it as repository changes