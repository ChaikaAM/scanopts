package com.epam.scanopts.core.git;

import com.epam.scanopts.core.OptionsFileDataHandler;
import com.epam.scanopts.util.FileUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.Date;
import java.util.List;

public interface GitRepositoryHandler {

    String FOLDER_NAME = OptionsFileDataHandler.INSTANCE.getRepositoryFolderName();
    File REPOSITORY_FOLDER = new File(FileUtil.jarFolder + File.separator + FOLDER_NAME);
    String COMMITER_NAME = OptionsFileDataHandler.INSTANCE.getCommiterName();
    String COMMITER_EMAIL = OptionsFileDataHandler.INSTANCE.getCommiterEmail();
    String BRANCH_NAME = OptionsFileDataHandler.INSTANCE.getBranchName();

    Logger logger = LogManager.getLogger(GitLocalRepositoryHandlerJGitImplementation.class.getSimpleName());

    default void prepareRepositoryAtStart() {
        logger.info("Preparing repository");
        mkdirRepositoryFolderIfNotExists();
        setGitInRepositoryFolder();
        setWorkingBranch();
        logger.info("Force refresh of options files in repository");
        copyOptionFilesToRepository(OptionsFileDataHandler.INSTANCE.getFilesFromMonitors());
        if (repositoryFilesChanged()) {
            commitChanges();
            pushChanges();
        }
    }

    void mkdirRepositoryFolderIfNotExists();

    void setGitInRepositoryFolder();

    void copyOptionFileToRepository(File optionFile);

    void copyOptionFilesToRepository(List<File> optionFiles);

    void deleteOptionFileFromRepository(File optionFile);

    boolean repositoryFilesChanged();

    void commitChanges();

    boolean setWorkingBranch();

    default String generateCommitMessage() {
        return "\"Commit by " + COMMITER_NAME + " at " + new Date().toString() + "\"";
    }

    void pushChanges();
}
