package com.epam.scanopts.core.git;

import com.epam.scanopts.util.FileUtil;
import com.epam.scanopts.util.SlackBot;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Implementation of GitHandler with usage of Eclipse JGit implementation
 * Pros - git could not be installed at server
 * Cons - not native git
 */
public class GitLocalRepositoryHandlerJGitImplementation implements GitRepositoryHandler {

    Git gitRepository;

    @Override
    public void setGitInRepositoryFolder() {
        try {
            logger.info("Trying to open " + REPOSITORY_FOLDER + " as git repository");
            gitRepository = Git.open(REPOSITORY_FOLDER);
            logger.info(REPOSITORY_FOLDER + " recognized as git repository");
        } catch (IOException openExistedGitException) {
            logger.info("No git repository found in " + REPOSITORY_FOLDER);
            logger.info("Initializing new one");
            try {
                gitRepository = Git.init().setDirectory(REPOSITORY_FOLDER).call();
                logger.info("Repository successfully inited");
            } catch (GitAPIException gitInitException) {
                logger.error("Unable to find any git repository in " + REPOSITORY_FOLDER + " or initializing new one into it");
                logger.error(openExistedGitException);
                logger.error(gitInitException);
                throw new RuntimeException("Unable to continue work without git repository");
            }
        }
    }

    @Override
    public void mkdirRepositoryFolderIfNotExists() {
        if (!REPOSITORY_FOLDER.exists()) {
            logger.info("No options repository found. Creating folder - " + REPOSITORY_FOLDER.getAbsolutePath());
            if (REPOSITORY_FOLDER.mkdir()) {
                logger.info("Folder \"" + REPOSITORY_FOLDER.getAbsolutePath() + "\" is created");
            } else {
                logger.error("There were problems during creating folder " + REPOSITORY_FOLDER.getAbsolutePath());
                logger.error("Application is unable to continue work");
                throw new RuntimeException("Unable to initialize repository folder");
            }
        } else if (REPOSITORY_FOLDER.exists() && REPOSITORY_FOLDER.isDirectory()) {
            logger.info("Folder for repository " + REPOSITORY_FOLDER.getAbsolutePath() + " is already exists. Using existing one");
        }
        if (REPOSITORY_FOLDER.exists() && REPOSITORY_FOLDER.isFile()) {
            logger.info("Repository should be folder, not file. Deleting " + REPOSITORY_FOLDER.getAbsolutePath());
            if (REPOSITORY_FOLDER.delete()) {
                logger.info("File with same name as repository is deleted");
                mkdirRepositoryFolderIfNotExists();
            } else throw new RuntimeException("Remove " + REPOSITORY_FOLDER + " file from application folder");
        }
    }


    @Override
    public void copyOptionFileToRepository(File optionFile) {
        try {
            FileUtil.copyFile(optionFile, REPOSITORY_FOLDER);
        } catch (IOException e) {
            logger.error("Unable to copy file " + optionFile.getAbsolutePath() + " to repository " + REPOSITORY_FOLDER.getAbsolutePath());
            throw new RuntimeException(e);
        }
    }

    @Override
    public void copyOptionFilesToRepository(List<File> optionFiles) {
        try {
            FileUtil.copyFiles(optionFiles, REPOSITORY_FOLDER);
        } catch (IOException e) {
            logger.error("Unable to copy files " + optionFiles.stream().map(a -> a.getAbsolutePath()).collect(Collectors.toList()) + " to repository " + REPOSITORY_FOLDER.getAbsolutePath());
            throw new RuntimeException(e);
        }
    }

    @Override
    public void deleteOptionFileFromRepository(File optionFile) {
        try {
            logger.info("Deleting file \"" + optionFile.getAbsolutePath() + "\" from repository " + REPOSITORY_FOLDER.getAbsolutePath());
            gitRepository.rm().addFilepattern(optionFile.getName()).call();
            logger.info("File is deleted from repository");
        } catch (GitAPIException e) {
            logger.error("Could not execute git rm at repository");
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean repositoryFilesChanged() {
        try {
            gitRepository.add().addFilepattern(".").call();
            return gitRepository.status().call().hasUncommittedChanges();
        } catch (GitAPIException e) {
            logger.error("Could not check git status");
            throw new RuntimeException(e);
        }
    }

    @Override
    public void commitChanges() {
        try {
            String commitMessage = generateCommitMessage();
            logger.info("Commiting changes in repository. Git message - " + commitMessage);
            RevCommit commit = gitRepository
                    .commit()
                    .setMessage(commitMessage)
                    .setCommitter(COMMITER_NAME, COMMITER_EMAIL)
                    .call();

            String diffWithPrevCommit = getDiffWithPrevCommit(commit);
            if (!diffWithPrevCommit.isEmpty()) {
                SlackBot.INSTANCE.sendMessageToChannel("Changes at " + BRANCH_NAME + "\n  :fixparrot:", diffWithPrevCommit);
            }

            logger.info("Commit created - " + commit.toString());
        } catch (GitAPIException e) {
            logger.error("Could not commit changes in repository");
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private String getDiffWithPrevCommit(RevCommit head) {
        RevCommit diffWith = head.getParent(0);

        String result = "";
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
             DiffFormatter diffFormatter = new DiffFormatter(baos)) {
            diffFormatter.setRepository(gitRepository.getRepository());
            for (DiffEntry entry : diffFormatter.scan(diffWith, head)) {
                diffFormatter.format(diffFormatter.toFileHeader(entry));
            }
            result = new String(baos.toByteArray());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean setWorkingBranch() {

        logger.info("Executing git log");
        boolean hasCommits = false;
        try {
            hasCommits = gitRepository.log().call().iterator().hasNext();
        } catch (NoHeadException ne) {
            logger.info("Unable to execute git log. It seems to be that there is no HEAD");
        } catch (GitAPIException e) {
            logger.error("Unable to execute git log, but git repository seems fine");
            throw new RuntimeException(e);
        }
        if (!hasCommits) {
            logger.info("Creating initial commit");
            commitChanges();
        } else logger.info("Commits exists. No need for initial commit");


        logger.info("Checking if branch " + BRANCH_NAME + " is already exists in repository " + REPOSITORY_FOLDER.getAbsolutePath());
        boolean isBranchExists = false;
        List<Ref> branches;
        try {
            branches = gitRepository.branchList().call();
        } catch (GitAPIException e) {
            logger.error("Could not check branches list in git folder " + REPOSITORY_FOLDER.getAbsolutePath());
            throw new RuntimeException("Unable for safe work with repository", e);
        }
        for (Ref branch : branches) {
            String[] branchAttributes = branch.getName().split("/");
            if (branchAttributes.length != 0 && branchAttributes[branchAttributes.length - 1].equalsIgnoreCase(BRANCH_NAME)) {
                isBranchExists = true;
            }
        }
        logger.info(isBranchExists ? "Branch " + BRANCH_NAME + " is already existed" : "Branch " + BRANCH_NAME + " is not existed");
        if (!isBranchExists) {
            logger.info("Creating branch " + BRANCH_NAME + " in repository in folder " + REPOSITORY_FOLDER.getAbsolutePath());
            try {
                gitRepository.branchCreate().setName(BRANCH_NAME).setStartPoint("master").call();
            } catch (GitAPIException e) {
                logger.error("Unable to create branch");
                throw new RuntimeException("Unable for safe work with repository ", e);
            }
        }

        logger.info("Checking out local branch " + BRANCH_NAME);
        try {
            gitRepository.checkout().setName(BRANCH_NAME).call();
        } catch (GitAPIException e) {
            logger.error("Could not check out " + BRANCH_NAME + " local branch");
        }
        return isBranchExists;
    }

    @Override
    @Deprecated
    public void pushChanges() {
        logger.info("Only local repository mode. Aint pushing no changes");
    }


}
