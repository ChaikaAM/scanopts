package com.epam.scanopts.core.git;

import com.epam.scanopts.core.OptionsFileDataHandler;
import org.eclipse.jgit.api.CreateBranchCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.PushResult;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

public final class GitLocalRemoteRepositoryHandlerJGitImplementation extends GitLocalRepositoryHandlerJGitImplementation {

    private String REMOTE_REPOSITORY_URL = OptionsFileDataHandler.INSTANCE.getRemoteRepositoryUrl();

    private CredentialsProvider credentialsProvider = new UsernamePasswordCredentialsProvider(OptionsFileDataHandler.INSTANCE.getUserName(), OptionsFileDataHandler.INSTANCE.getPassword());

    @Override
    public void setGitInRepositoryFolder() {
        try {
            logger.info("Trying to open " + REPOSITORY_FOLDER + " as git repository");
            gitRepository = Git.open(REPOSITORY_FOLDER);
            logger.info(REPOSITORY_FOLDER + " recognized as git repository");
            checkLocalRepositoryBelongsToRemoteRepository();
        } catch (IOException openExistedGitException) {
            logger.info("No git repository found in " + REPOSITORY_FOLDER);
            logger.info("Clonning " + REMOTE_REPOSITORY_URL + " into " + REPOSITORY_FOLDER);
            //TODO It should be also done if branch in existing local repository is not the same as in options file
            try {
                //first try to clone branch from options in case if it exists
                gitRepository = Git.cloneRepository().setCredentialsProvider(credentialsProvider).setURI(REMOTE_REPOSITORY_URL).setBranch(BRANCH_NAME).setDirectory(REPOSITORY_FOLDER).call();
                logger.info("Repository is cloned and successfully inited with branch " + getCurrentBranch());
            } catch (GitAPIException e) {
                try {
                    //download main repo branch if branch from options is not exist
                    gitRepository = Git.cloneRepository().setCredentialsProvider(credentialsProvider).setURI(REMOTE_REPOSITORY_URL).setDirectory(REPOSITORY_FOLDER).call();
                    logger.info("Repository is cloned and successfully inited with branch " + getCurrentBranch());
                } catch (GitAPIException cloningRemoteGitException) {
                    logger.error("Unable to find any git repository in " + REPOSITORY_FOLDER + " or clone remote " + REMOTE_REPOSITORY_URL + " into it");
                    logger.error(openExistedGitException);
                    logger.error(cloningRemoteGitException);
                    throw new RuntimeException("Unable to continue work without git repository");
                }
            }
        }
    }

    @Override
    public boolean setWorkingBranch() {
        logger.info("Pulling changes from current branch - " + getCurrentBranch());
        try {
            PullResult pulled = gitRepository.pull().setCredentialsProvider(credentialsProvider).call();
            logger.info("Branch " + getCurrentBranch() + " pulled " + (pulled.isSuccessful() ? "successfully" : "failed"));
        } catch (GitAPIException e) {
            logger.error("Could not pull changes from " + REMOTE_REPOSITORY_URL + " - " + getCurrentBranch());
        }

        logger.info("Current branch is " + getCurrentBranch() + ". Branch from options file - " + BRANCH_NAME);
        if (BRANCH_NAME.equalsIgnoreCase(getCurrentBranch())) {
            logger.info("Current branch is already the same as described in option files and pulled.");
            return true;
        } else {
            logger.info("Current branch is not the same as described in options file. Checking if " + BRANCH_NAME + " already exists");
            boolean remoteBranchExist = isRemoteBranchExist(BRANCH_NAME);
            String startPoint = getBranchParent(BRANCH_NAME) != null ? getBranchParent(BRANCH_NAME).getName() : getCurrentBranch();
            if (remoteBranchExist) {
                logger.info(BRANCH_NAME + "is already exists. Checking it out. Parent - " + startPoint);
                try {
                    gitRepository
                        .checkout()
                        .setName(BRANCH_NAME)
                        .setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.TRACK)
                        .setStartPoint("origin/" + BRANCH_NAME)
                        .call();
                    gitRepository.pull().setCredentialsProvider(credentialsProvider).call();
                } catch (GitAPIException e) {
                    e.printStackTrace();
                }
            } else {
                logger.info(BRANCH_NAME + " branch is not existing. Creating new one from " + startPoint);
                try {
                    gitRepository
                        .checkout()
                        .setCreateBranch(true)
                        .setName(BRANCH_NAME)
                        .setUpstreamMode(CreateBranchCommand.SetupUpstreamMode.TRACK)
                        .setStartPoint(startPoint)
                        .call();
                } catch (GitAPIException e) {
                    logger.error("Could not initialize remote branch " + BRANCH_NAME + " in " + REMOTE_REPOSITORY_URL);
                    throw new RuntimeException(e);
                }
            }
            return false;
        }
    }

    @Override
    public void commitChanges() {
        super.commitChanges();
    }

    @Override
    public void pushChanges() {
        logger.info("Pushing changes to " + getCurrentBranch() + " in " + REMOTE_REPOSITORY_URL);
        try {
            Iterable<PushResult> pushResults = gitRepository
                .push()
                .setCredentialsProvider(credentialsProvider)
                .call();
            pushResults.forEach(pushResult -> logger.info(pushResult.getRemoteUpdates()));
            logger.info("Push passed successfully");
        } catch (GitAPIException e) {
            logger.error("Failed to push!!! Some problems occured. Try to do it manually");
            logger.error(e);
        }
    }

    private String getCurrentBranch() {
        try {
            return gitRepository.getRepository().getBranch();
        } catch (IOException e) {
            throw new RuntimeException("Unable to detect current branch", e);
        }
    }

    private Ref getBranchParent(String branchName) {
        final Map<String, Ref> allRefs = gitRepository.getRepository().getAllRefs();
        for (String branch : allRefs.keySet()) {
            String[] branchAttributes = branch.split("/");
            if (branchAttributes.length != 0 && branchAttributes[branchAttributes.length - 1].equalsIgnoreCase(branchName)) {
                return allRefs.get(branch).getLeaf();
            }
        }
        return null;
    }

    private boolean isRemoteBranchExist(String branchName) {
        logger.info("Checking if branch " + BRANCH_NAME + " is already exists in repository " + REPOSITORY_FOLDER.getAbsolutePath());
        boolean isBranchExists = false;
        Set<String> branches;
        branches = gitRepository.getRepository().getAllRefs().keySet();
        for (String branch : branches) {
            String[] branchAttributes = branch.split("/");
            if (branchAttributes.length != 0 && branchAttributes[branchAttributes.length - 1].equalsIgnoreCase(branchName)) {
                isBranchExists = true;
                break;
            }
        }
        return isBranchExists;
    }

    private void checkLocalRepositoryBelongsToRemoteRepository() {
        logger.info("Option file describes remote repository as " + REMOTE_REPOSITORY_URL);
        String currentUrl = gitRepository.getRepository().getConfig().getString("remote", "origin", "url");
        logger.info("Current local repository belongs to remote - " + currentUrl);
        if (!currentUrl.equalsIgnoreCase(REMOTE_REPOSITORY_URL)) {
            logger.error("Local repository is not the same as remote");
            logger.error("If you want to use another remote repository remove existing local because those are distinct projects");
            throw new RuntimeException("Different repositories - local and remote");
        } else logger.info("Local repository ");
    }
}
