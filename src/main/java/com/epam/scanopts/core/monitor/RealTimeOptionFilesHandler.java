package com.epam.scanopts.core.monitor;

import com.epam.scanopts.core.OptionsFileDataHandler;
import com.epam.scanopts.core.file.FileChangeType;
import com.epam.scanopts.core.file.FileMonitor;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.epam.scanopts.ScanoptsApplication.COMMON_LOGGER;

public class RealTimeOptionFilesHandler extends FileMonitorHandler {

    public static final RealTimeOptionFilesHandler INSTANCE = new RealTimeOptionFilesHandler();

    private ExecutorService service;

    private volatile boolean isRunning;
    private volatile boolean stateChanged;

    private List<FileMonitor> fileMonitors = OptionsFileDataHandler.INSTANCE.getFileMonitors(true);

    private RealTimeOptionFilesHandler() {

    }

    @Override
    public void init() {
        if (!isRunning) {
            gitRepositoryHandler.prepareRepositoryAtStart();
            service = Executors.newFixedThreadPool(fileMonitors.size() + 1);
            COMMON_LOGGER.info("-------SCANNING FOR OPTION FILES CHANGES-------");
            isRunning = true;
            fileMonitors.forEach(fm -> service.execute(() -> {
                    while (isRunning) {
                        COMMON_LOGGER.info("Waiting for changes in file {}", fm.getFile().getAbsolutePath());
                        final FileChangeType fileChangeType = fm.getChangeEvent();
                        boolean isFileInRepositoryChanged = writeFileChangeToLocalRepository(fileChangeType, fm);
                        if (isFileInRepositoryChanged) {
                            stateChanged = isFileInRepositoryChanged;
                        }
                    }
                })
            );
            service.execute(() -> {
                while (isRunning) {
                    try {
                        TimeUnit.MINUTES.sleep(1L);
                    } catch (InterruptedException e) {
                        COMMON_LOGGER.error("Unable to maintain interval in commit/push operations");
                    }
                    if (stateChanged && gitRepositoryHandler.repositoryFilesChanged()) {
                        COMMON_LOGGER.info("Changes in local repository files happened");
                        gitRepositoryHandler.commitChanges();
                        gitRepositoryHandler.pushChanges();
                        stateChanged = false;
                    }
                }
            });
        }

    }

    @Override
    public void destroy() {
        isRunning = false;
        service.shutdown();
    }
}
