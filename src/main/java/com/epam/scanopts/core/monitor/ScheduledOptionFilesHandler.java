package com.epam.scanopts.core.monitor;

import com.epam.scanopts.core.OptionsFileDataHandler;
import com.epam.scanopts.core.file.FileChangeType;
import com.epam.scanopts.core.file.FileMonitor;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.epam.scanopts.ScanoptsApplication.COMMON_LOGGER;

public final class ScheduledOptionFilesHandler extends FileMonitorHandler {

    public static final ScheduledOptionFilesHandler INSTANCE = new ScheduledOptionFilesHandler();

    private ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();

    private boolean isRunning;

    private List<FileMonitor> fileMonitors = OptionsFileDataHandler.INSTANCE.getFileMonitors(false);
    private Long compareDelayMinutes;
    private Runnable scanningTask = () -> {
        COMMON_LOGGER.info("-------SCANNING FOR OPTION FILES CHANGES-------");
        fileMonitors.forEach(fm -> {
            final FileChangeType fileChangeType = fm.getChangeEvent();
            writeFileChangeToLocalRepository(fileChangeType, fm);
        });
        if (gitRepositoryHandler.repositoryFilesChanged()) {
            gitRepositoryHandler.commitChanges();
            gitRepositoryHandler.pushChanges();
        }
    };

    private ScheduledOptionFilesHandler() {
    }

    public FileMonitorHandler setCompareDelay(Long compareDelayMinutes) {
        INSTANCE.compareDelayMinutes = compareDelayMinutes;
        return INSTANCE;
    }

    @Override
    public void init() {
        if (!isRunning) {
            gitRepositoryHandler.prepareRepositoryAtStart();
            service.scheduleAtFixedRate(scanningTask, 0, compareDelayMinutes, TimeUnit.MINUTES);
            isRunning = true;
        }
    }

    @Override
    public void destroy() {
        if (isRunning) {
            service.shutdown();
        }
    }
}
