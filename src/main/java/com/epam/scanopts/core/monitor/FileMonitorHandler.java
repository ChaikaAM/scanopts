package com.epam.scanopts.core.monitor;

import com.epam.scanopts.core.file.FileChangeType;
import com.epam.scanopts.core.file.FileMonitor;
import com.epam.scanopts.core.git.GitLocalRemoteRepositoryHandlerJGitImplementation;
import com.epam.scanopts.core.git.GitRepositoryHandler;

import static com.epam.scanopts.ScanoptsApplication.COMMON_LOGGER;

public abstract class FileMonitorHandler {

    public final GitRepositoryHandler gitRepositoryHandler = new GitLocalRemoteRepositoryHandlerJGitImplementation();

    abstract public void init();

    abstract public void destroy();

    boolean writeFileChangeToLocalRepository(FileChangeType fileChangeType, FileMonitor fm) {
        if (fileChangeType != FileChangeType.NOT_CHANGED) {
            COMMON_LOGGER.info("Option file has been changed - " + fm.getFile().getAbsolutePath() + " - " + fileChangeType);
            if (fileChangeType == FileChangeType.MODIFIED) {
                gitRepositoryHandler.copyOptionFileToRepository(fm.getFile());
            } else if (fileChangeType == FileChangeType.CREATED) {
                gitRepositoryHandler.copyOptionFileToRepository(fm.getFile());
            } else if (fileChangeType == FileChangeType.DELETED) {
                gitRepositoryHandler.deleteOptionFileFromRepository(fm.getFile());
            }
            return true;
        } else {
            COMMON_LOGGER.info("No changes found for " + fm.getFile().getAbsolutePath());
            return false;
        }
    }
}
