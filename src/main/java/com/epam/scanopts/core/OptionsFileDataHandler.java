package com.epam.scanopts.core;

import com.epam.scanopts.core.file.BlockingFileMonitor;
import com.epam.scanopts.core.file.FileMonitor;
import com.epam.scanopts.core.file.NonBlockingFileMonitor;
import com.epam.scanopts.util.EcriptionAesUtil;
import com.epam.scanopts.util.FileUtil;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static com.epam.scanopts.ScanoptsApplication.COMMON_LOGGER;

public final class OptionsFileDataHandler {

    private static final String OPTIONS_FILE = "options.properties";

    public static OptionsFileDataHandler INSTANCE = new OptionsFileDataHandler();

    private List<File> files = new ArrayList<>();

    private String repositoryFolderName;
    private String commiterName;
    private String commiterEmail;
    private String remoteRepositoryUrl;
    private String userName;
    private String password;
    private String branchName;

    private OptionsFileDataHandler() {
        initDataFromOptionsFile(true);  //TODO Switching from blocking or nonblocking monitors should be passed out
    }

    public List<FileMonitor> getFileMonitors(boolean blockingFileMonitors) {
        return files
                .stream()
                .map(file -> file.getAbsolutePath())
                .map(filePath -> blockingFileMonitors ? new BlockingFileMonitor(filePath) : new NonBlockingFileMonitor(filePath))
                .collect(Collectors.toList());
    }

    public List<File> getFilesFromMonitors() {
        return files;
    }

    public String getRepositoryFolderName() {
        return repositoryFolderName;
    }

    public String getCommiterName() {
        return commiterName;
    }

    public String getCommiterEmail() {
        return commiterEmail;
    }

    public String getRemoteRepositoryUrl() {
        return remoteRepositoryUrl;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getBranchName() {
        return branchName;
    }

    private void initDataFromOptionsFile(boolean blockingFileMonitors) {
        Properties properties = new Properties();

        try (FileReader fr = new FileReader(FileUtil.jarFolder.getAbsolutePath() + File.separator + OPTIONS_FILE)) {
            properties.load(fr);

            String optionFiles = properties.getProperty("optionFiles");

            Arrays.stream(optionFiles.split(",")).forEach(filePath -> files.add(new File(filePath)));
            //other settings parsing
            repositoryFolderName = properties.getProperty("repositoryFolderName");
            commiterName = properties.getProperty("commiterName");
            commiterEmail = properties.getProperty("commiterEmail");
            remoteRepositoryUrl = properties.getProperty("remoteRepositoryUrl");
            String ENV_HOST_NAME = System.getenv("NODEFQHOST");
            branchName = ENV_HOST_NAME != null ? (ENV_HOST_NAME.toLowerCase()) : "unknown_host";
            String encryptedCredentials = properties.getProperty("remoteRepositorySecret");
            Map.Entry<String, String> encodedCredentials = EcriptionAesUtil.INSTANCE.decodeCredentials(encryptedCredentials);
            userName = encodedCredentials.getKey();
            password = encodedCredentials.getValue();
        } catch (IOException e) {
            COMMON_LOGGER.error("There were error reading options file " + OPTIONS_FILE + " and parsing it");
            throw new RuntimeException(e);
        } catch (Exception e) {
            COMMON_LOGGER.error("Could not parse options file " + OPTIONS_FILE);
            throw new RuntimeException(e);
        }
    }
}
