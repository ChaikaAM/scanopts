package com.epam.scanopts.core.console;

import org.apache.commons.cli.CommandLine;

import static com.epam.scanopts.ScanoptsApplication.COMMON_LOGGER;

/**
 * Wrapper for Commons-CLI command line object
 */
public final class ConsoleCommand {

    private final CommandLine commandLine;

    /**
     * Default delay used in case if was not set in command line args
     */
    static final Long DEFAULT_COMPARE_DELAY = 15L;

    ConsoleCommand(CommandLine commandLine) {
        this.commandLine = commandLine;
    }

    public final boolean isStartCommand() {
        return commandLine.hasOption(Arguments.START.getOption().getOpt());
    }

    public final boolean isHelpCommand() {
        return commandLine.hasOption(Arguments.HELP.getOption().getOpt());
    }

    public final Long getCompareDelay() {
        if (!commandLine.hasOption(Arguments.TIME.getOption().getOpt())) {
            COMMON_LOGGER.info("No interval of comparing options was set in command line. Using default value - " + DEFAULT_COMPARE_DELAY);
            return DEFAULT_COMPARE_DELAY;
        }
        String optionValue = commandLine.getOptionValue(Arguments.TIME.getOption().getOpt());
        try {
            Long compareDelay = Long.parseLong(optionValue);
            COMMON_LOGGER.info("Getting entered compare delay from command line - " + compareDelay);
            return compareDelay;
        } catch (NumberFormatException nfe) {
            nfe.printStackTrace();
            COMMON_LOGGER.error("Argument \"" + optionValue + "\" for time parameter in command line entered incorrectly. Should be integer number. Using default value " + DEFAULT_COMPARE_DELAY);
            return DEFAULT_COMPARE_DELAY;
        }
    }
}
