package com.epam.scanopts.core.console;

import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public final class ConsoleHandler {

    public static final ConsoleHandler INSTANCE = new ConsoleHandler();

    private final Logger logger = LogManager.getLogger(ConsoleHandler.class.getSimpleName());

    private final Options commandLineOpts;
    private final HelpFormatter helpFormatter;
    private final CommandLineParser cmdLineParser;

    private ConsoleHandler() {
        commandLineOpts = new Options();
        Arrays.stream(Arguments.values())
            .map(Arguments::getOption)
            .forEach(commandLineOpts::addOption);
        cmdLineParser = new DefaultParser();
        helpFormatter = new HelpFormatter();
    }

    public final ConsoleCommand transformToConsoleCommand(String[] args) throws ParseException {
        if (args.length == 0) {
            logger.error("You need to enter arguments to use this program");
            printHelp();
            throw new RuntimeException("No arguments entered");
        }
        try {
            CommandLine commandLine = cmdLineParser.parse(commandLineOpts, args);
            return new ConsoleCommand(commandLine);
        } catch (ParseException e) {
            logger.error("Wrong argument line format. Help: ");
            printHelp();
            throw e;
        }
    }

    public final void printHelp() {
        helpFormatter.printHelp("java -jar scanopts.jar", commandLineOpts);
    }
}
