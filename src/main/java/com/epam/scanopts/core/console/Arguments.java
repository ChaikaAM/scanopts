package com.epam.scanopts.core.console;

import org.apache.commons.cli.Option;

public enum Arguments {
    START("s", "start", false, "start options monitoring"),
    TIME("t", "time", true, "set interval of comparing options in minutes (e.g. -t 300). Default - " + ConsoleCommand.DEFAULT_COMPARE_DELAY),
    HELP("h", "help", false, "print help");

    private final Option option;

    Arguments(String shortName, String longName, boolean needsValue, String descriptionl) {
        this.option = new Option(shortName, longName, needsValue, descriptionl);
    }

    public final Option getOption() {
        return option;
    }
}
