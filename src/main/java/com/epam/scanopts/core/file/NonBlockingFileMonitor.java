package com.epam.scanopts.core.file;

public final class NonBlockingFileMonitor extends FileMonitor {

    public NonBlockingFileMonitor(String filePath) {
        super(filePath);
    }

    @Override
    public final FileChangeType getChangeEvent() {
        watchKey = watchService.poll();
        return findFileChangeInWatchKey(watchKey);
    }
}
