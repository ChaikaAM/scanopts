package com.epam.scanopts.core.file;

import static com.epam.scanopts.ScanoptsApplication.COMMON_LOGGER;

public final class BlockingFileMonitor extends FileMonitor {

    public BlockingFileMonitor(String filePath) {
        super(filePath);
    }

    /**
     * Implemetation blocks thread until any changes with file would happen
     *
     * @return FileChangeType - see FileChangeType enum
     */
    @Override
    public FileChangeType getChangeEvent() {
        try {
            watchKey = watchService.take();
            COMMON_LOGGER.info("File monitor of file {} detected changes in its directory", file.getAbsolutePath());
        } catch (InterruptedException e) {
            COMMON_LOGGER.error("There were some problems with thread monitoring file " + file.getAbsolutePath());
            COMMON_LOGGER.error("Closing application");
            throw new RuntimeException(e);
        }
        return findFileChangeInWatchKey(watchKey);
    }
}
