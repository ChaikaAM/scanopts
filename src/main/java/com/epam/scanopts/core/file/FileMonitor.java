package com.epam.scanopts.core.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

import static com.epam.scanopts.ScanoptsApplication.COMMON_LOGGER;

public abstract class FileMonitor {

    final File file;

    WatchService watchService;
    WatchKey watchKey;

    public FileMonitor(String filePath) {
        this.file = new File(filePath);
        if (!file.exists()) {
            COMMON_LOGGER.error("Could not find file {} described in option_files.json", filePath);
            throw new RuntimeException("Could not initialize FileMonitor for file " + filePath + ". File does not exists");
        }
        Path fileFolder = file.toPath().getParent();
        try {
            watchService = FileSystems.getDefault().newWatchService();
            watchKey = fileFolder.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_CREATE);
        } catch (IOException e) {
            COMMON_LOGGER.error("Could not initialize monitor for folder " + fileFolder.toFile().getAbsolutePath());
            throw new RuntimeException(e);
        }
    }

    public abstract FileChangeType getChangeEvent();

    public final File getFile() {
        return file;
    }

    FileChangeType findFileChangeInWatchKey(WatchKey watchKey) {
        FileChangeType changeType = FileChangeType.NOT_CHANGED;
        if (watchKey != null) {
            for (WatchEvent watchEvent : watchKey.pollEvents()) {
                if (((Path) watchEvent.context()).endsWith(file.getName())) {
                    changeType = FileChangeType.fromWatchEvent(watchEvent);
                }
            }
            watchKey.reset();
        }
        return changeType;
    }
}
