package com.epam.scanopts.core.file;

import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;

public enum FileChangeType {
    NOT_CHANGED,
    MODIFIED,
    CREATED,
    DELETED,
    UNRECOGNIZED_CHANGE;

    public static FileChangeType fromWatchEvent(WatchEvent watchEvent) {
        if (watchEvent != null) {
            final String nameEventKind = watchEvent.kind().name();
            if (nameEventKind.equals(StandardWatchEventKinds.ENTRY_MODIFY.name())) {
                return MODIFIED;
            } else if (nameEventKind.equals(StandardWatchEventKinds.ENTRY_CREATE.name())) {
                return CREATED;
            } else if (nameEventKind.equals(StandardWatchEventKinds.ENTRY_DELETE.name())) {
                return DELETED;
            } else return UNRECOGNIZED_CHANGE;
        } else return NOT_CHANGED;
    }
}
