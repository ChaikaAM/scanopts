package com.epam.scanopts.util;

import com.epam.scanopts.ScanoptsApplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import static com.epam.scanopts.ScanoptsApplication.COMMON_LOGGER;

public final class BannerUtil {

    private BannerUtil() {
    }

    public static void printBanner() {
        try (InputStream inputStream = ScanoptsApplication.class.getClassLoader().getResourceAsStream("banner");
             InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
             BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        ) {
            bufferedReader.lines().forEach(line -> COMMON_LOGGER.info(line));
        } catch (IOException e) {
            COMMON_LOGGER.info("Unable to print banner");
        } finally {
            COMMON_LOGGER.info(System.lineSeparator());
            COMMON_LOGGER.info("SCANOPTS application - scan option files for changes and see difference in git repository ");
            COMMON_LOGGER.info(System.lineSeparator());
        }
    }
}
