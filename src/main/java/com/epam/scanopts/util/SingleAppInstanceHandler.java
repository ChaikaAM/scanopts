package com.epam.scanopts.util;

import com.epam.scanopts.ScanoptsApplication;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

import static com.epam.scanopts.ScanoptsApplication.COMMON_LOGGER;

public final class SingleAppInstanceHandler {

    private static final String LOCK_FILE_NAME = "apprun.lock";

    private File file;
    private FileChannel channel;
    private FileLock fileLock;

    private static SingleAppInstanceHandler INSTANCE;

    private SingleAppInstanceHandler() {
    }

    public static synchronized SingleAppInstanceHandler init() throws IOException {
        INSTANCE = new SingleAppInstanceHandler();
        INSTANCE.file = initFile();
        return INSTANCE;
    }

    public final boolean occupyLock() throws IOException {
        channel = new RandomAccessFile(file, "rw").getChannel();
        fileLock = channel.tryLock();

        if (fileLock != null) {
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    fileLock.release();
                    channel.close();
                    file.delete();
                    ScanoptsApplication.fileMonitorHandler.destroy();
                } catch (Exception e) {
                    COMMON_LOGGER.error("Unable to remove lock file: " + file.getAbsolutePath(), e);
                } catch (NoClassDefFoundError error) {
                    COMMON_LOGGER.error("While closing app could not deinitialize one of the classes. Probably it was not initialized. Message - " + error.getMessage());
                }
            }));
            return true;
        } else {
            channel.close();
            return false;
        }
    }

    private static File initFile() throws IOException {
        File lockFile = new File(FileUtil.jarFolder + File.separator + LOCK_FILE_NAME);
        if (!lockFile.exists()) {
            boolean created = lockFile.createNewFile();
            if (!created) throw new RuntimeException("Could not instantiate lock file " + lockFile.getAbsolutePath());
        }
        if (!lockFile.isFile() || lockFile.isDirectory()) {
            boolean isDeleted = lockFile.delete();
            if (isDeleted) {
                return initFile();
            } else
                throw new RuntimeException(lockFile.getAbsolutePath() + " was found but it is not a file. Could not delete it");
        }
        return lockFile;
    }
}
