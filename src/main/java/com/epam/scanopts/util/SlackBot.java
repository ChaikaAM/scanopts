package com.epam.scanopts.util;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public final class SlackBot {

    private final RestTemplate restTemplate = new RestTemplate();

    private static final Logger logger = LogManager.getLogger(FileUtil.class.getSimpleName());
    private static final String BOT_NAME = "BDC4161C5";
    private static final String CHANNEL = "server_options";
    //    String name = "scanopts";
    private static final String token = "xoxb-291559498823-453209656080-meoaApcKPks1X1MMnqjdzmaf";


    public static final SlackBot INSTANCE = new SlackBot();

    private SlackBot() {
    }

    public String sendMessageToChannel(String message, String body) {
        if (body != null && !body.isEmpty()) {
            message = message + "```" + body + "```";
            if (message.length() > 1000) message = message.substring(0, 997) + "```";
        } else if (message.length() > 1000) message = message.substring(0, 1000);
        return SlackBot.INSTANCE.sendPostRequest("chat.postMessage", String.class, "channel", CHANNEL, "text", message, "as_user", "true");
    }

    private <T> T sendPostRequest(String method, Class<T> responseType, String... args) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(getMap(args), headers);
        String url = getAuthenticatedUrl(method);
        logger.info("Sending to method \"{}\" body - {}. Full url - {}", method, request, url);
        return restTemplate.postForObject(url, request, responseType);
    }

    private String getAuthenticatedUrl(String method) {
        return "https://slack.com/api/" + method + "?token=" + token;
    }

    private MultiValueMap getMap(String... params) {
        if (params.length % 2 != 0) throw new IllegalArgumentException("Every key has to have a value");
        MultiValueMap<String, String> result = new LinkedMultiValueMap<>();
        for (int i = 0; i < params.length; i += 2) {
            result.add(params[i], params[i + 1]);
        }
        return result;
    }
}
