package com.epam.scanopts.util;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.AbstractMap;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import static com.epam.scanopts.ScanoptsApplication.COMMON_LOGGER;

/**
 * Class used for generating key in build folder
 */
public final class EcriptionAesUtil {

    public static final EcriptionAesUtil INSTANCE = new EcriptionAesUtil();

    private static final String filePath = FileUtil.jarFolder.getAbsolutePath() + File.separator + "key";

    private EcriptionAesUtil() {

    }

    public String generateKeyAndEncryptedCreds(String credentials) throws Exception {
        if (credentials.split(":").length != 2)
            throw new RuntimeException("Credentials format should be like username:password");
        final SecretKey secretKey = generateSecretKey();
        writeKeyToFile(secretKey, filePath);
        COMMON_LOGGER.info("Key to be stored near jar file is created in " + filePath);

        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);

        byte[] bytesAes = cipher.doFinal(credentials.getBytes());
        byte[] bytesAesBase64 = Base64.getEncoder().encode(bytesAes);
        String encrypted = new String(bytesAesBase64);
        COMMON_LOGGER.info("Encrypted from entered creds string - " + encrypted);
        return encrypted;
    }

    public Map.Entry<String, String> decodeCredentials(String encodedCreds) throws Exception {
        final SecretKey secretKey = readKeyFromFile(filePath);

        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);

        byte[] bytesAes = Base64.getDecoder().decode(encodedCreds.getBytes());
        byte[] bytes = cipher.doFinal(bytesAes);

        String[] creds = new String(bytes).split(":");
        if (creds.length != 2)
            throw new RuntimeException("Something wrong with decripted string, it could not be safely parsed");
        AbstractMap.SimpleEntry<String, String> decodedCredentials = new HashMap.SimpleEntry<>(creds[0], creds[1]);
        COMMON_LOGGER.info("Credentials has been succesfully decrypted");
        return decodedCredentials;
    }

    private SecretKey generateSecretKey() throws NoSuchAlgorithmException {
        return KeyGenerator.getInstance("AES").generateKey();
    }

    private SecretKey readKeyFromFile(String filePath) throws IOException {
        byte[] keyb = Files.readAllBytes(Paths.get(filePath));
        return new SecretKeySpec(keyb, "AES");
    }

    private void writeKeyToFile(SecretKey secretKey, String filePath) throws IOException {
        try (FileOutputStream out = new FileOutputStream(filePath)) {
            byte[] keyb = secretKey.getEncoded();
            out.write(keyb);
        }
    }
}
