package com.epam.scanopts.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;

public final class FileUtil {

    private static final Logger logger = LogManager.getLogger(FileUtil.class.getSimpleName());

    public static File jarFolder;

    static {
        try {
            File jarFile = new File(FileUtil.class.getProtectionDomain().getCodeSource().getLocation().toURI());
            jarFolder = jarFile.getParentFile();
            if (!jarFolder.isDirectory()) throw new RuntimeException();
        } catch (URISyntaxException e) {
            throw new RuntimeException("could not instantiate jar location");
        }
    }

    private FileUtil() {
    }

    public static void copyFile(File file, File destinationFolder) throws IOException {
        File destinationFile = getFileInRepositoryFolder(file, destinationFolder);
        if (!file.exists()) throw new RuntimeException();
        logger.info("Copying file \"" + file.getAbsolutePath() + "\" to repository " + destinationFile.getAbsolutePath());
        Files.copy(file.toPath(), destinationFile.toPath(), StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.REPLACE_EXISTING);
        logger.info("File is copied to repository");
    }

    public static void copyFiles(List<File> files, File destinationFolder) throws IOException {
        for (File file : files) {
            copyFile(file, destinationFolder);
        }
    }

    /**
     * Should be used only without JGit library implementation of GitRepositoryHandler,
     * because of that "add ." in library does not index manually removed files.
     * In case of JGit library fiels should be deleted by gitRepository.rm().addFilepattern("myfile.txt").call();
     */
    @Deprecated
    public static void deleteSameFile(File file, File destinationFolder) throws IOException {
        File destinationFile = getFileInRepositoryFolder(file, destinationFolder);
        if (!destinationFile.exists()) {
            logger.error(destinationFile.getAbsolutePath() + " could not be deleted in repository. No file founded");
            return;
        }
        logger.info("Deleting file \"" + file.getAbsolutePath() + "\" from repository " + destinationFile.getAbsolutePath());
        Files.delete(destinationFile.toPath());
        logger.info("File is deleted from repository");
    }

    private static File getFileInRepositoryFolder(File file, File folder) {
        if (!file.isFile() || file.isDirectory()) {
            throw new RuntimeException(file.getAbsolutePath() + " is not a file");
        }
        if (folder.isFile() || !folder.isDirectory()) {
            throw new RuntimeException(folder.getAbsolutePath() + " is not a folder");
        }
        return new File(folder.getAbsolutePath() + File.separator + file.getName());
    }
}
