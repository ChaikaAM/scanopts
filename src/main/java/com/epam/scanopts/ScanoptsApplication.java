package com.epam.scanopts;

import com.epam.scanopts.core.OptionsFileDataHandler;
import com.epam.scanopts.core.console.ConsoleCommand;
import com.epam.scanopts.core.console.ConsoleHandler;
import com.epam.scanopts.core.monitor.FileMonitorHandler;
import com.epam.scanopts.core.monitor.RealTimeOptionFilesHandler;
import com.epam.scanopts.util.BannerUtil;
import com.epam.scanopts.util.FileUtil;
import com.epam.scanopts.util.SingleAppInstanceHandler;
import com.epam.scanopts.util.SlackBot;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.jgit.api.errors.JGitInternalException;
import org.eclipse.jgit.internal.storage.file.LockFile;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public final class ScanoptsApplication {

    public static Logger COMMON_LOGGER = LogManager.getLogger(ScanoptsApplication.class);

    public static FileMonitorHandler fileMonitorHandler;

    public static void main(String[] args) throws InterruptedException {

        addShutdownHook();

        SlackBot.INSTANCE.sendMessageToChannel(":white_check_mark: `" + OptionsFileDataHandler.INSTANCE.getBranchName() + "` *scanopts is starting...*", null);

        BannerUtil.printBanner();

        try {
            startScanning(args);
            SlackBot.INSTANCE.sendMessageToChannel(":heavy_check_mark: `" + OptionsFileDataHandler.INSTANCE.getBranchName() + "` *has been started...*", null);
        } catch (JGitInternalException jge) {
            handleJgitException(jge);
        } catch (Exception e) {
            SlackBot.INSTANCE.sendMessageToChannel(":ballot_box_with_check: `" + OptionsFileDataHandler.INSTANCE.getBranchName() + "` is *failing : *\n", e.toString());
            waitBeforeFail();
        }
    }

    private static void startScanning(String[] args) throws IOException, ParseException {
        if (!SingleAppInstanceHandler.init().occupyLock()) {
            COMMON_LOGGER.error("Only one application instance could be running. Please close/kill another instance of scanopts to run the new one");
            throw new RuntimeException("Lock file is blocked by another instance of scanopts");
        }

        final ConsoleCommand consoleCommand = ConsoleHandler.INSTANCE.transformToConsoleCommand(args);

//        fileMonitorHandler = ScheduledOptionFilesHandler.INSTANCE.setCompareDelay(consoleCommand.getCompareDelay());  //It was decided to use real time monitor
        fileMonitorHandler = RealTimeOptionFilesHandler.INSTANCE;

        if (consoleCommand.isStartCommand()) {
            fileMonitorHandler.init();
        } else if (consoleCommand.isHelpCommand()) {
            ConsoleHandler.INSTANCE.printHelp();
        }
    }

    private static void waitBeforeFail() throws InterruptedException {
        SlackBot.INSTANCE.sendMessageToChannel(":ballot_box_with_check: `" + OptionsFileDataHandler.INSTANCE.getBranchName() + "` is *failed*\n" +
                "Pausing for 10 minutes before restart ... ", "To avoid slack API blocking waiting for 10 minutes before restart");
        TimeUnit.MINUTES.sleep(10L);
    }

    private static void addShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(() ->
                SlackBot.INSTANCE.sendMessageToChannel(":ballot_box_with_check: `" + OptionsFileDataHandler.INSTANCE.getBranchName() + "` scanopts is *forced to be closed...*", null))
        );
    }

    private static void handleJgitException(JGitInternalException jge) throws InterruptedException {
        SlackBot.INSTANCE.sendMessageToChannel(":ballot_box_with_check: `" + OptionsFileDataHandler.INSTANCE.getBranchName() + "` is *failing : *\n" +
                "It seems to be that server has been restarted during git operation... Trying to delete ../scanopts/git folder on server or try to remove it or index.lock manually \n", jge.toString());
        boolean localGitDeleted = RealTimeOptionFilesHandler.INSTANCE.gitRepositoryHandler.REPOSITORY_FOLDER.delete();
        if (localGitDeleted) {
            SlackBot.INSTANCE.sendMessageToChannel(":ballot_box_with_check: `" + OptionsFileDataHandler.INSTANCE.getBranchName() + "` is *failing : *\n" +
                    "Local git folder deleted and will be recovered on the next launch\n", null);
        } else {
            SlackBot.INSTANCE.sendMessageToChannel(":ballot_box_with_check: `" + OptionsFileDataHandler.INSTANCE.getBranchName() + "` is *failing : *\n" +
                    "Unable to delete ./scanopts/git folder on server. \n *ADVICE* Try to remove it or index.lock manually \n", null);
            waitBeforeFail();
        }
    }

}
